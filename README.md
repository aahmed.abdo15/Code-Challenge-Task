#Code Challenge Task
## Goal

Clone this repo and build a simple dictionary key/value store script using only core NodeAPI and ECMAScript 5 or 6.
Store the key/value dictionary using filesystem.
The client should be a standalone terminal tool.
Commit and track your work history in a new GitLab repo. Once finished email the link to your repo.
## Installation

`$ npm install -g`

## Test Install (Lists API Options)

`$ store`

## Store API

`$ store add mykey myvalue`

`$ store list`

`$ store get mykey`

`$ store remove mykey`

`$ store clear`

## Test Plan
1. Adds keys and values.  
2. Lists all keys and values.
3. Handles duplicate keys. 
4. Retrieves value based on keys, handles duplicates.
5. Removes key/value based on key.

##Bonus
1.Write clean, modular and testable code.
2.Instead of running node store.js alter the runtime so it can be run as ./store.
3.Add ability to deploy in Docker container.
4.Add GitLab compatible CI/CD to perform a server deploy. 
